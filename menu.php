<!DOCTYPE html>
<html>
    <head>
        <title>Menu</title>
        <?php 
            $cont = file_get_contents("includes.php"); 
            echo $cont;
        ?>
    </head>
    <body>
        <?php 
            $cont = file_get_contents("header.php"); 
            echo $cont;
        ?>
        <div id="contenidoMenu">
            <h1 class="subTitulo"><u>Menú esmorzar</u></h1>
            <div id="menuComida">
                
                <?php
                    include 'items.php';
                    
                    for($i=0;$i<count($patio);$i++){

                        echo "<div class=\"item\" name=\"item\">";
                        echo "  <div class=\"checkItem\">";
                        echo "      <input type=\"checkbox\" name=\"checkItem\">";
                        echo "  </div>";
                        echo "  <div class=\"presentacion\">";
                        echo "      <div class=\"nombre\" name=\"nombre\">".$patio[$i]["nombre"]."</div>"; 
                        echo "      <img src=\"".$patio[$i]["imagen"]."\" height=\"42\" width=\"42\">";
                        echo "      <div class=\"precio\" name=\"precio\">".$patio[$i]["precio"]."€</div>";
                        echo "  </div>";
                        echo "  <div class=\"cantidad\">";
                        echo "      <img src=\"img/sumar.png\" height=\"42\" name=\"mas\">";
                        echo "      <div name=\"numero\" class=\"numero\">1</div>";
                        echo "      <img src=\"img/restar.png\" height=\"42\" name=\"menos\">";                     
                        echo "  </div>";
                        echo "</div>";
                    }

                ?>
            </div>
            <div id="menuAlmuerzo">

                <?php
                    include 'items.php';
                    
                    for($i=0;$i<count($comida);$i++){

                        echo "<div class=\"item\" name=\"item\">";
                        echo "  <div class=\"checkItem\">";
                        echo "      <input type=\"checkbox\" name=\"checkItem\">";
                        echo "  </div>";
                        echo "  <div class=\"presentacion\">";
                        echo "      <div class=\"nombre\" name=\"nombre\">".$comida[$i]["nombre"]."</div>"; 
                        echo "      <img src=\"".$comida[$i]["imagen"]."\" height=\"42\" width=\"42\">";
                        echo "      <div class=\"precio\" name=\"precio\">".$comida[$i]["precio"]."€</div>";
                        echo "  </div>";
                        echo "  <div class=\"cantidad\">";
                        echo "      <img src=\"img/sumar.png\" height=\"42\" name=\"mas\">";
                        echo "      <div name=\"numero\" class=\"numero\">1</div>";
                        echo "      <img src=\"img/restar.png\" height=\"42\" name=\"menos\">";                     
                        echo "  </div>";
                        echo "</div>";
                    }

                ?>
            </div>
            <form action="formulari.php" method="POST" id="formularioPedido" >
            </form>
            <button id="submitComanda">Comprar</button> 
        </div>

            
        <script>

            //Oculta los los botones de cantidad para que si no esta seleccionado no se muestre
            var checkItems=document.getElementsByName("checkItem");
            
            for(let i=0;i<checkItems.length;i++){
                document.getElementsByName("checkItem")[i].addEventListener("click",function (){
                    
                    if(document.getElementsByName("checkItem")[i].checked){
                        document.getElementsByClassName("cantidad")[i].style.display="flex";
                    }else{
                        document.getElementsByClassName("cantidad")[i].style.display="none";
                    }
                });
            }


            //Control evento boton menos

            var botonesMenos=document.getElementsByName("menos");
            for(let i=0;i<botonesMenos.length;i++){
                document.getElementsByName("menos")[i].addEventListener("click",function (){
                    
                    if(parseInt(document.getElementsByName("numero")[i].innerHTML)>1){
                        document.getElementsByName("numero")[i].innerHTML=parseInt(document.getElementsByName("numero")[i].innerHTML)-1;
                    }

                });

            }

            //Control evento boton mas
            var botonesMas=document.getElementsByName("mas");
            for(let i=0;i<botonesMas.length;i++){
                document.getElementsByName("mas")[i].addEventListener("click",function (){

                    document.getElementsByName("numero")[i].innerHTML=parseInt(document.getElementsByName("numero")[i].innerHTML)+1;
                });

            }
            
            //Constante de la hora a la que se mostrara otro menu
            const horario=12;

            //Conprueba el menu que debe evaluar dependiendo de la hora
            var menu;
            if(new Date().getHours()>horario){
                menu=document.getElementById("menuAlmuerzo");
            }else{
                menu=document.getElementById("menuComida");
            }
            var checkItems=menu.getElementsByTagName("input");
                


            //Controla el evento de submit para registrar la comanda y mandarla a la siguiente pagina
                document.getElementById("submitComanda").type="submit";
                document.getElementById("submitComanda").addEventListener("click",function(){

                    //Controla que algun elemento a sido seleccionado

                    var pedidoPosible=false;

                    for(let i=0;i<checkItems.length;i++){
                        if(checkItems[i].checked)
                            pedidoPosible=true;                
                    }
                    
                    if(pedidoPosible){
                        console.log("Pedido posible");
                        for(let i=0;i<checkItems.length;i++){
                        if(checkItems[i].checked && menu.getElementsByClassName("numero")[i].innerHTML!=0){
                            var input=document.createElement("input");
                            input.style.display="none";
                            input.setAttribute("type", "text");
                            input.setAttribute("name",i);
                            input.value=document.getElementsByName("numero")[i].innerHTML;
                            document.getElementById("formularioPedido").appendChild(input);
                        }

                        }
                        document.getElementById("formularioPedido").submit();

                    }else{
                        alert("Tienes que elegir algun producto");
                    }
                    
                        


                });


            //Oculta los dos menus para mostrarlos deacuerdo a la hora del dia
            if(new Date().getHours()>horario){
                document.getElementById("menuAlmuerzo").style.display="block";
                document.getElementById("menuComida").style.display="none";
            }else{
                document.getElementById("menuComida").style.display="block";
                document.getElementById("menuAlmuerzo").style.display="none";
            }


            //Reverifica campos seleccionados
            for(let i=0;i<checkItems.length;i++){
                if(checkItems[i].checked){
                    menu.getElementsByClassName("cantidad")[i].style.display="flex";
                }
                                         
            }

        </script>
        <footer> 
            <div id="footerEspecial">© 2019 Copyright:
                <a class="link" href="http://labs.iam.cat/~a18NeiGutCha/proyecto/"> CantinaPedralbes.com</a>
            </div>
        </footer>
    </body>
</html>