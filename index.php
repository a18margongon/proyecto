<!DOCTYPE html>
    <html>
        <head>
            <title>Cantina Pedralbes</title>
            <?php 
                 $cont = file_get_contents("includes.php"); 
                 echo $cont;
            ?>
        </head>
        <body>
            <?php 
            $cont = file_get_contents("header.php"); 
            echo $cont;
            ?>
            <div class="menu">
                <div class="menuItem" id="horario">Horari</div>
                <div class="menuItem" id="ubicacion">On estem?</div>
                <div class="menuItem" id="nosotros">Qui som?</div>
                <div class="menuItem" id="admin">log admin</div>
            </div>
            <div class="contenido">
                <div class="imagen">
                        <img src="img/comida2.jpg" alt="Comida" width="600" height="400">
                </div>

                <div>
                    <div class="texto" id="fecha">
                        <p>Mati: 08:00H a 12:00H</p> <p>Tarda: 14:00H a 21:00H</p>
                    </div>
                    <div class="texto" id="lugar">
                        <p>Av. d'Esplugues, 40, 08034 Barcelona</p>
                    </div>
                    <div class="texto" id="informacion">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                    </div>
                </div>
               
            </div>
            <div class="boton">
                <button type="button"  onClick="window.location.href='menu.php'">Ir al menu</button>
            </div>
            <script>
                function ocultarTodos(){
                        let array=document.getElementsByClassName("texto")
                    for (let index = 0; index < array.length; index++) {
                        array[index].style.display="none";                    
                    }
                }
                ocultarTodos()
                let opcionesmenu=document.getElementsByClassName("menuItem");
                let textos=document.getElementsByClassName("texto");


                //Añade los eventos de mostrar y ocultar a los botones del menu 

                for (let index = 0; index < opcionesmenu.length; index++) {                    
                   

                    const element = opcionesmenu[index];
                    console.log(element);
                    opcionesmenu[index].addEventListener("click", function(){

                        //Controla que si hace click en el boton de login no redireccion al comandas.txt
                        if(index==opcionesmenu.length-1){
                            console.log("entro");
                            window.location.href='privado/';
                        }else{
                            ocultarTodos();
                            textos[index].style.display="block";
                        }

                    })
                }
            </script>
            <?php 
                $cont = file_get_contents("footer.php"); 
                echo $cont;
            ?>
        </body>
    </html>