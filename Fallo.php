<!DOCTYPE html>
    <html>
        <head>
            <title>Error</title>
            <?php 
                $cont = file_get_contents("includes.php"); 
                echo $cont;
            ?>
        </head>
        <body>
            <?php 
                $cont = file_get_contents("header.php"); 
                echo $cont;
            ?>
            <div class="confirmacion">
                <div class="textoConfirmacion">
                Tu compra no ha sido realizada
                </div>
                <div>
                    <img src="img/wrong.png" alt="tick" width="50" height="50">
                </div>
            </div>
            <div>
                <button type="button" style="float:right;" onClick="window.location.href='index.php' ">Volver al menu principal</button>
            </div>
            <?php 
                $cont = file_get_contents("footer.php"); 
                echo $cont;
            ?>
        </body>
    </html>