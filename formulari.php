<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Confirmar comanda</title>
    <?php 
        $cont = file_get_contents("includes.php");
        echo $cont;

        
     ?>
     
    
</head>
<body>
    <?php 
        $cont = file_get_contents("header.php"); 
        echo $cont;
     ?>

    <div id="contenedor">
        <div class="comanda">
           <h1>Informació Comanda</h1>
           <hr/>
                <img id ="imgComanda"src="img/Comida-basura.jpg" >
           <hr/>
           <h3>Descripció</h3>
           <?php
                include "items.php";
                $num = count($_POST);
                $preuFinal = 0;
                if (localtime()["2"]<12){
                    foreach($_POST as $nombre=>$cantidad){
                        echo "<p>". "x" .$cantidad ." " . $patio[$nombre]["nombre"]. " " . $patio[$nombre]["precio"] ." €" . "</p>";
                        $preuFinal += $patio[$nombre]["precio"]*$cantidad;
                    }
                }
                else {
                    foreach($_POST as $nombre=>$cantidad){
                        echo "<p>". "x" .$cantidad ." " . $comida[$nombre]["nombre"]. " " . $comida[$nombre]["precio"] ." €" . "</p>";
                        $preuFinal += $comida[$nombre]["precio"]*$cantidad;
                    }
                }

                echo "<p>" . "<b>Preu final: </b>". "$preuFinal". " €"."<p>";
           ?>

            <button type="button" id="retornoAMenu">Ir al menu</button>
        </div>
            <div class="divformulari">
                <h1>Introdueix les teves dades</h1>
                <p>Per tal de completar la comanda necessitem les teves dades</p>
                <form action="registroComanda.php" method="post" name="registro" id="registro">
                    <fieldset>
                        <legend>Introdueix informació</legend>
                        <div>
                            <label class="label">Nom</label>
                            <input class="input" name="nom" type="text" id="nom" size="25"> 
                        </div>
                        <br/>
                        <div>
                            <label class="label">Telèfon</label>
                            <input class="input"  name="telefon" type="text" id="telefon" size="25"> 
                        </div>
                        <br/>
                        <div>
                            <label class="label">Correu Electrònic</label>
                            <input class="input" name="correu" type="text" id="correu" size="25"> 
                        </div>
                        <div>
                            <input type="hidden" name="arrayComanda">
                        </div>
                    </fieldset>
                </form>
                <button type="submit" id="submit">Completar Comanda</button>
            </div> 
    </div>
    <?php 
        $cont = file_get_contents("footer.php"); 
        echo $cont;
     ?>
</body>

<script type="text/javascript">
        
        document.getElementsByName("arrayComanda")[0].value='<?php echo serialize($_POST);?>';

        document.getElementById("submit").addEventListener("click", function(){
            let txtNom = document.getElementById("nom").value;
            let txtTelefon = document.getElementById("telefon").value;
            let txtCorreu = document.getElementById("correu").value;
            let domini = txtCorreu.substring(txtCorreu.length-17,txtCorreu.length);
            let nomCorreu = txtCorreu.substring(txtCorreu[0],txtCorreu.length-17);
 
            document.getElementById("submit").type="submit";
    
            if (txtNom == null || txtNom.length == 0){
                alert('ERROR: Nom no vàlid');
                
            }else if (txtTelefon.length != 9) {
                alert('ERROR: telèfon no vàlid');
                
            }else if (txtCorreu == null || txtCorreu.length == 0 || domini != "@inspedralbes.cat" || nomCorreu.includes("@")){
                alert('ERROR: Correu no vàlid');
            }else{

               document.getElementById("registro").submit();
               
            }
            

        });
        //Controla el evento del boton "Ir al menu"
        console.log(document.getElementById("retornoAMenu"));
            document.getElementById("retornoAMenu").addEventListener("click",function (){
                console.log("Click");
                history.back();
            });
            

     </script>


</html>